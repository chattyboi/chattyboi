#!/bin/sh
DESTINATION="$HOME/chattyboi"
LAUNCH_ON_COPY=true

if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  echo "This script will install ChattyBoi on your system."
  echo "Files will be copied to $DEFAULT_DEST"
  echo "Any existing files will be overwritten."
  exit 0
fi

echo "Copying to $DESTINATION"
mkdir -p "$DESTINATION"
ls ./*[^venv]
cp -r ./*[^venv] "$DESTINATION"
python -m venv "$DESTINATION/venv"
$DESTINATION/venv/bin/python -m pip install --upgrade pip -qq
$DESTINATION/venv/bin/python -m pip install -r "$DESTINATION/requirements.txt" -qq

if [ $LAUNCH_ON_COPY = true ]; then
	$DESTINATION/venv/bin/python $DESTINATION/main.py
fi
