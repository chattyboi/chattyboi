import json
from PySide2.QtCore import QObject


class Chatter(QObject):
	__instances__ = {}
	
	def __new__(cls, id, database=None, parent=None):
		if database is None:
			# TODO: retrieve database from current global profile
			pass
		if (id, database) in cls.__instances__:
			return cls.__instances__[(id, database)]
		self = super().__new__(cls, id, database, parent=parent)
		self._initialized = False
		cls.__instances__[(id, database)] = self
		return self
	
	def __init__(self, id, database, parent=None):
		if not self._initialized:
			super().__init__(parent=parent)
			self._initialized = True
		self.id = id
		self.database = database
	
	def __str__(self):
		if self.exists():
			return self.primary_nickname
		return '[[invalid-user]]'
	
	def __eq__(self, other):
		return self.id == other.id and self.database == other.database and self.exists()
	
	def exists(self):
		c = self.database.cursor()
		c.execute('SELECT rowid FROM chatters WHERE id = ?', (self.id,))
		return bool(c.fetchone())
	
	def create(self, nicknames=None):
		c = self.database.cursor()
		c.execute(
			'INSERT INTO chatters (id, nicknames) VALUES (?, ?)',
			(self.id, json.dumps(nicknames or []))
		)
	
	@property
	def primary_nickname(self):
		return self.nicknames[0]
	
	@property
	def nicknames(self):
		c = self.database.cursor()
		c.execute('SELECT nicknames FROM chatters WHERE id = ?', (self.id,))
		return json.loads(c.fetchone()[0])
	
	@nicknames.setter
	def nicknames(self, value):
		self.database.cursor().execute(
			'UPDATE chatters SET nicknames = ? WHERE id = ?',
			(json.dumps(value), self.id)
		)
