from PySide2.QtCore import QObject
from PySide2.QtWidgets import QApplication

import classes

events = {}
chats = []

def register_event(event, callbacks=None):
	events[event.name] = event
	if callbacks:
		for fn in callbacks:
			event.triggered.connect(fn)


def register_chat(chat, event_whitelist=None):
	chats.append(chat)
	def _on_message_received(message):
		event_list = (
			events.values()
			if event_whitelist is None else
			(e[1] for e in events.items() if e[0] in event_whitelist)
		)
		for event in event_list:
			event.react_to(message, chat)
	chat.message_received.connect(on_messsage_received)


def register_callback(event_name, callback):
	events[event_name].triggered.connect(callback)


def create_simple_event(name, check, default_data=None) -> classes.Event:
	event = classes.Event(name, default_data)
	
	def react_to(self, message, source):
		if data := check(message, source):
			self.triggered.emit(default_data | {'source': source, 'message': message})
	
	event.react_to = react_to
