from PySide2.QtCore import Signal, QObject


class Event(QObject):
	"""
	An instance of Event represents a specific type of event that can happen when any message is received.
	For example, a command that creates a file when the bot receives a message starting with '!file'
	would be represented as an instance of this class.
	
	Extensions are expected to subclass Event to implement their own checks for when an event
	should be triggered in response to a message. Event management is done through the ``app`` module,
	where extensions can register new events to be added to the global list. Registered events will
	react to any message received from any chat except those that explicitly ignore certain events;
	see the ``Chat`` class for more info.
	
	To register a callback, connect it to the ``triggered`` signal. This can also be done by
	providing an event name to ``app.register_callback()``, which is more useful most of the time.
	"""
	triggered = Signal(dict)
	
	def __init__(self, name, default_data=None, parent=None):
		super().__init__(parent=parent)
		self.name = name
		self.default_data = default_data or {}
		
	def trigger(self, **data):
		self.triggered.emit(self.default_data | data)
		
	def react_to(self, message, source):
		"""
		If the message should trigger the event, then it's triggered. Otherwise, nothing happens.
		"""
		self.triggered.emit(self.default_data)
