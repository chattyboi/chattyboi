from typing import Optional, List, Tuple

import json
import sqlite3
from PySide2.QtCore import QObject

import utils
import config
import classes


class DatabaseWrapper(QObject):
	"""
	Wrapper around a profile's SQLite3 database, which provides several ChattyBoi-specific
	helper functions. The underlying ``sqlite3.Connection`` is stored in the ``connection`` field.
	
	``DatabaseWrapper``s created from the same path are singletons. ``DatabaseWrapper``s are
	equal (as in the ``==`` operator) if they share the same connection object.
	"""
	__instances__ = {}
	
	def __new__(self, path, *args, parent=None, **kwargs):
		path = path.resolve()
		if path in cls.__instances__:
			return cls.__instances__[path]
		self = super().__new__(cls, path, *args, parent, **kwargs)
		self._initialized = False
		cls.__instances__[path] = self
		return self
	
	def __init__(self, path, *args, parent=None, **kwargs):
		super().__init__(parent=parent)
		if not self._initialized:
			self.connection = sqlite3.Connection(path, *args, **kwargs)
			self.path = path
			self.myself = self.chatter(config.get_setting('database/default_self_nickname'))
			self._initialized = True
		
	def __eq__(self, other):
		return self.connection is other.connection
		
	def __hash__(self):
		return id(self.connection)
	
	def cursor(self):
		"""
		Get a ``Cursor`` object from the SQLite3 connection.
		"""
		return self.connection.cursor()
	
	def new_chatter(self, nicknames) -> classes.Chatter:
		"""
		Create a database entry and return a new ``Chatter`` object with the given nickname.
		If the nickname already exists in the database, ``RuntimeError`` is raised.
		"""
		for nickname in nicknames:
			if f := self.find_chatter(nickname):
				raise RuntimeError(f'A user with the nickname {nickname} already exists (ID {f.id}).')
		c = self.cursor()
		c.execute(
			'INSERT INTO chatters (nicknames) VALUES (?)',
			(json.dumps(nicknames),)
		)
		return classes.Chatter(
			id=c.execute('SELECT id FROM chatters WHERE rowid = last_insert_rowid()').fetchone()[0],
			database=self
		)
	
	def find_chatter(self, nickname) -> Optional[classes.Chatter]:
		"""
		Return the ``Chatter`` object if an entry matching the given nickname exists, and ``None`` otherwise.
		"""
		c = self.cursor()
		c.execute('SELECT id FROM user_info WHERE nicknames LIKE ?', (f'%{json.dumps(nickname)}%',))
		try:
			id = c.fetchone()[0]
			return classes.Chatter(id, self)
		except TypeError:
			return None
		
	def chatter(self, nickname) -> classes.Chatter:
		"""
		Return the corresponding ``Chatter`` object if an entry matching the nickname exists,
		and a new Chatter otherwise.
		
		Synonym for ``(self.find_chatter(nickname) or self.new_chatter([nickname]))``.
		"""
		if f := self.find_chatter(nickname):
			return f
		return self.new_chatter([nickname])
	
	def validate(self):
		"""
		Validate the database by creating new data if it's missing
		or raising ``RuntimeError`` if it exists but is invalid.
		"""
		pass
	
	def setup(self):
		"""
		Like ``validate()``, but assumes a blank database; existing data is not checked for validity.
		"""
		pass
	
	def add_extension(self, name, columns: List[Tuple[str, type]], modify_existing=True):
		if not name.isidentifier():
			raise ValueError(f'Invalid extension table name: {name}')
		table_name = f'{config.PROFILE_DATABASE_EXTENSION_TABLE_PREFIX}{name}'
		converted_columns = {n: utils.python_type_to_sql_type(t) for n, t in columns}
		c = self.cursor()
		if len(c.execute('SELECT * FROM pragma_table_info(?)', (table_name,)).fetchall()) == 0:
			c.execute(
				f'CREATE TABLE {table_name} (
					id INTEGER NOT NULL,
					{"(?) (?), " * len(columns)}
					FOREIGN KEY (id) REFERENCES chatters(id)
				);',
				(table_name, *(y for x in converted_columns for y in x))
			)
		elif modify_existing:
			existing_columns = {
				r['name']: r['type']
				for r in c.execute('SELECT * FROM pragma_table_info(?)', (table_name,)).fetchall()
			}
			if existing_columns == converted_columns:
				return
			for col, datatype in converted_columns.items():
				if col not in existing_columns:
					c.execute('ALTER TABLE (?) ADD COLUMN (?) (?)', (table_name, col, datatype))
				elif datatype != existing_columns[col]:
					raise RuntimeError(
						f'Attempted to modify the existing extension table for {name} by adding '
						f'new columns, but the column {col} already exists and its datatype '
						f'{existing_columns[col]} does not match the given type {datatype}'
					)
	
	def remove_extension(self, name):
		c = self.cursor()
		c.execute('DROP TABLE IF EXISTS (?)', (f'{config.PROFILE_DATABASE_EXTENSION_TABLE_PREFIX}{name}',))
