def python_type_to_sql_type(t: type) -> str:
	return {
		None: 'NULL',
		NoneType: 'NULL',
		int: 'INT',
		float: 'REAL',
		str: 'TEXT'
	}.get(t, 'BLOB')
