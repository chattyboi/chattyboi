# Copyright 2020 Illia Boiko <ilyaviaik at gmail dot com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import abc
from PySide2.QtCore import QObject
from .chatter import Chatter


class MessageContent(abc.ABC):
	@abc.abstractmethod
	def __str__(self):
		pass


MessageContent.register(str)
MessageContent.register(bytes)


class Message(QObject):
	"""
	A single message received from a chat.
	"""
	def __init__(self, timestamp: int, author: Chatter, content: MessageContent, parent=None):
		super().__init__(parent=parent)
		self.timestamp = timestamp
		self.author = author
		self.content = content
	
	def __str__(self):
		return str(self.content)
