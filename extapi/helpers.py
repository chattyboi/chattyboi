from typing import Callable, Optional, Dict
from classes import MessageContent, Message, Chatter, Chat, Event

__all__ = (
	'create_simple_event'
)


def create_simple_event(
		name: str,
		check: Callable[[Message, Chat], Optional[Dict]],
		default_data: Optional[Dict] = None
	) -> Event:
	event = Event(name, default_data)
	
	def react_to(self, message, source):
		if data := check(message, source):
			self.triggered.emit(default_data | {'source': source, 'message': message})
	
	event.react_to = react_to 
