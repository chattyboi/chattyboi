from pathlib import Path
from PySide2.QtCore import QSettings

import config
from .wrapper import DatabaseWrapper


class Profile:
	def __init__(self, path):
		self.path = Path(path)
		self.properties = config.PROFILE_DEFAULT_PROPERTIES
		self.properties_file = self.path / config.PROFILE_PROPERTIES_FILENAME
		self.database_file = self.path / config.PROFILE_DATABASE_FILENAME
		self.storage_directory = self.path / config.PROFILE_STORAGE_DIRECTORY
		
		# Validate the profile's path
		if not self.path.is_dir():
			if self.path.exists():
				raise RuntimeError(f'The path "{str(self.path)}" is not a valid directory.')
			self.setup()
			return
		
		# Validate and load the properties file
		if not self.properties_file.is_file():
			if self.properties_file.exists():
				raise RuntimeError(f'The path "{str(self.properties_file)}" is not a valid file.')
			self.properties_file.touch()
		with open(self.properties_file) as f:
			try:
				self.properties.update(json.load(f))
			except json.decoder.JSONDecodeError as e:
				print(f'Error when loading "{str(self.properties_file)}": {e}')
				print(f'Contents were backed up to "{self.properties_file}.bkp"')
				print(f'Using default profile properties.')
				with open(str(self.properties_file) + '.bkp', 'w') as b:
					b.write(f.read())
		
		# Validate the storage directory and its subdirectories
		if not self.storage_directory.is_dir():
			if self.storage_directory.exists():
				raise RuntimeError(f'The path "{str(self.storage_directory)}" is not a valid directory.')
			self.storage_directory.mkdir()
		for extension in self.properties[extensions]:
			path = self.storage_directory / extension
			if not path.is_dir():
				if path.exists():
					raise RuntimeError(f'The path "{str(path)}" is not a valid directory.')
				path.mkdir()
				
		# Validate the database
		if not self.database_file.is_file():
			if self.database_file.exists():
				raise RuntimeError(f'The path "{str(self.database_file)}" is not a valid file.')
			self.database_file.touch()
			self.database_wrapper().setup()
		else:
			self.database_wrapper().validate()
				
	def database_wrapper(self) -> DatabaseWrapper:
		return DatabaseWrapper(self.database_file)
			
	def setup(self):
		self.path.mkdir(parents=True)
		with open(self.properties_file, 'w') as f:
			json.dump(f, self.properties)
		self.database_file.touch()
		self.storage_directory.mkdir()
		for extension in self.properties[extensions]:
			(self.storage_directory / extension).mkdir()
		self.database_wrapper().setup()
				
	def save_properties(self, path):
		with open(path, 'w') as f:
			json.dump(f, self.properties)
