import logging
from pathlib import Path
from PySide2.QtCore import QSettings

### Start of configuration constants ###

QT_APP_NAME = 'ChattyBoi'
QT_ORG_NAME = 'selplacei'

LOG_FORMAT = '%(asctime)s:%(name)s: [%(levelname)s] %(message)s'
LOG_DATEFMT = '%H:%M:%S'
LOG_LEVEL = logging.DEBUG

USERDATA_DIRECTORY = Path.home() / '.chattyboi'
PROFILES_DIRECTORY = USERDATA_DIRECTORY / 'profiles'

PROFILE_PROPERTIES_FILENAME = 'profile.json'
PROFILE_DATABASE_FILENAME = 'database.db'
PROFILE_STORAGE_DIRECTORY = 'storage'
PROFILE_DEFAULT_PROPERTIES = {
	'name': 'Unnamed',
	'description': None,
	'created_on': None,
	'extensions': []
}
PROFILE_DATABASE_EXTENSION_TABLE_PREFIX = 'extension_table_'

DEFAULT_SETTINGS = {
	'database/default_self_nickname': '__chattyboi_self__'
}

### End of configuration constants ###


settings = QSettings(QSettings.UserScope, QT_APP_NAME, QT_ORG_NAME)


def get_setting(key, default=None):
	return settings.value(key, default)


def set_setting(key, value):
	settings.setValue(key, value)


def reset_settings():
    settings.clear()
    for k, v in DEFAULT_SETTINGS.values():
		settings.setValue(k, v)
