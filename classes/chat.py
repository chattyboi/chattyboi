from PySide2.QtCore import Signal, QObject
from .chatter import Chatter
from .message import Message, MessageContent


class Chat(QObject):
	"""
	Representation of an underlying API that supports sending and receiving messages.
	Note that an instance of Chat is an actual chat stream, and not a chat type -
	for example, if connecting to something like IRC or Discord, it would make sense
	to have separate Chat instances for every text channel.
	
	Chat management is done through the ``app`` module, where extensions can register
	new chats to be added to the global list. It is possible for specific Chat instances to
	be explicitly ignored by certain ``Event``s. See ``app.register_chat()`` for more info.
	"""
	message_received = Signal(Message)
	
	def __init__(self, name=None, cache_size=100, parent=None):
		super().__init__(parent=parent)
		self.name = name
		self.cache_size = cache_size
		# Newer messages are at the end of the ``self.messages`` list.
		self.messages = []
		
	def __str__(self):
		return self.name
	
	async def send(self, content: MessageContent):
		"""
		Send content to this chat (abstract method).
		This method will not automatically add the message to ``self.messages``;
		the underlying API is expected to send back a regular message response
		corresponding to what was sent here.
		"""
		pass
	
	async def reply(self, content: MessageContent, target: Chatter):
		"""
		Send content directed towards a specific chatter in this chat.
		The default implementation assumes that MessageContent is str or bytes,
		and simply adds the target's name to the sent message.
		"""
		await self.send(str(target) + ': ' + content)
		
	def _on_message_received(self, message: Message):
		"""
		This method should be invoked by the wrapper of the underlying API whenever a message
		is received, even if it's from the chatter corresponding to this program.
		The wrapper should construct a Message object and pass it here, but shouldn't do anything
		else to the Chat object. The ``message_received`` signal is emitted here automatically.
		"""
		while len(self.messages) >= self.cache_size:
			self.messages.pop(0)
		self.messages.append(message)
		self.message_received.emit(message)
